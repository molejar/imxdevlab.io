This report was generated on Tue Oct 3 14:35:15 UTC 2017

 |**PDF file** |**Last modification**|
|[IMX50CE.pdf](http://www.nxp.com/docs/en/errata/IMX50CE.pdf)                 | Mon Jun 8 10:23:45 2015                  |
|[IMX28CE.pdf](http://www.nxp.com/docs/en/errata/IMX28CE.pdf)                 | Thu Sep 6 09:35:41 2012                  |
|[MCIMX27CE.pdf](http://www.nxp.com/docs/en/errata/MCIMX27CE.pdf)                 | Fri Aug 9 09:12:09 2013                  |
|[IMX53CE.pdf](http://www.nxp.com/docs/en/errata/IMX53CE.pdf)                 | Tue May 19 11:12:41 2015                  |
|[IMX23CE.pdf](http://www.nxp.com/docs/en/errata/IMX23CE.pdf)                 | Mon Jan 28 21:52:06 2013                  |
|[IMX35CE.pdf](http://www.nxp.com/docs/en/errata/IMX35CE.pdf)                 | Wed May 28 13:52:41 2014                  |
|[MCIMX51CE.pdf](http://www.nxp.com/docs/en/errata/MCIMX51CE.pdf)                 | Thu Feb 23 12:02:56 2012                  |
|[MC9328MXSCE.pdf](http://www.nxp.com/docs/en/errata/MC9328MXSCE.pdf)                 | Thu Jun 7 09:47:53 2007                  |
|[MCIMX31CE.pdf](http://www.nxp.com/docs/en/errata/MCIMX31CE.pdf)                 | Thu Jan 24 14:42:56 2013                  |
|[MC9328MX21CE.pdf](http://www.nxp.com/docs/en/errata/MC9328MX21CE.pdf)                 | Wed Nov 7 12:57:29 2012                  |
