This report was generated on Tue Oct 3 14:35:15 UTC 2017

 |**PDF file** |**Last modification**|
|[IMX6ULLCE.pdf](http://www.nxp.com/docs/en/errata/IMX6ULLCE.pdf)                 | Wed May 31 11:54:15 2017                  |
|[IMX6SXCE.pdf](http://www.nxp.com/docs/en/errata/IMX6SXCE.pdf)                 | Thu Apr 7 10:46:50 2016                  |
|[IMX6ULCE.pdf](http://www.nxp.com/docs/en/errata/IMX6ULCE.pdf)                 | Wed May 31 11:55:06 2017                  |
|[IMX6DQCE.pdf](http://www.nxp.com/docs/en/errata/IMX6DQCE.pdf)                 | Thu Aug 18 09:58:37 2016                  |
|[IMX6SLCE.pdf](http://www.nxp.com/docs/en/errata/IMX6SLCE.pdf)                 | Wed Jul 16 13:18:27 2014                  |
|[IMX6SDLCE.pdf](http://www.nxp.com/docs/en/errata/IMX6SDLCE.pdf)                 | Wed Aug 17 10:28:29 2016                  |
