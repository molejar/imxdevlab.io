---
layout: archive
permalink: /tag-cloud/
title: "All the Tags"
author_profile: false
---

{% include base_path %}
{% include group-by-array collection=site.posts field="tags" %}

{% for tag in group_names %}
  {% assign posts = group_items[forloop.index0] %}
  {{ tag }}
{% endfor %}

