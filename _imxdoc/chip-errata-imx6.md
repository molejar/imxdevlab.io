---
title: "i.MX6 SoC Errata"
excerpt: "A page to list all the links to the online Errata for the imx6 chips"
---
{% include imxdoc/rm-introduction.md %}
{% include imxdoc/ce-imx6-table.md %}

* The file `IMX6DQCE.pdf` applies to i.MX 6Dual/6Quad, i.MX 6DualPlus/6QuadPlus.
