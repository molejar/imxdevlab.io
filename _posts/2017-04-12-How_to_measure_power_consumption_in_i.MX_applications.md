---
title: How to measure power consumption in i.MX applications
date: 2017-04-12 14:01:49
author: vanessa
categories: [ Tutorial ]
tags: [ power, current, consumption, IMX6Q, IMX6QP, IMX6UL, IMX7D, IMX6SX, IMX6SL]
---
This post is intended to show a method to determine the power consumption in
i.MX boards and to share some relevant information about this topic, such as
operating points of each SoC.

# Operating points

The i.MX processors have several internal power supply domains. As an example,
the connectivity of the supply rails and the distribution of the internal power
domains for the i.MX 6Dual/6Quad SoC can be seen in the image below[^1]:

<center><img src="{{ base_path }}/images/supply_rails.png" alt="i.MX 6Dual/6Quad Power Rails" width="800"/></center>
<center><figcaption>i.MX 6Dual/6Quad Power Rails</figcaption></center>

The power measurements are taken mainly for the following power supply domains,
which consume the majority of the internal power of the processor:

* VDDARM_IN: ARM platform's supply;
* VDDSOC_IN: peripheral supply;
* VDDHIGH_IN: source of PLLs, DDR pre-drivers, PHYs, and some other circuitries.

Each power rail has a current sense resistor that allows the current to be measured.
These resistors can be found by analyzing the boards schematics. Each sense resistor
is usually placed between the PMIC output and the corresponding VDD rail, in the
i.MX6Q Sabre-SD case it is the R27 resistor for the ARM current and the R21 resistor
for SOC current.

To determine the power consumed on each power rail it is necessary to calculate
the current that flows through these sense resistors and then multiply it by the
power rail's voltage.

The probe points for some of the NXP's i.MX6x and i.MX7x boards can be found at
the table below[^2]:

|      Boards      |  ARM Current  | ARMCAP Voltage |  SOC Current  | SOCCAP Voltage | VDDHIGH Current | VDDHIGH Voltage |
|:----------------:|:-------------:|:--------------:|:-------------:|:--------------:|:---------------:|:---------------:|
|  i.MX6Q Sabre-SD |      R27      |   C679 C669    |      R21      |    C682 C667   |       SH17      |       C654      |
| i.MX6QP Sabre-SD |      SH5      |    C679 C669   |      R27      |   C682 C667    |       SH17      |       C654      |
| i.MX6QP Sabre-AI |     SH502     |    C620 C653   |      SH4      |      C691      |        -        |       C57       |
|    i.MX6SL EVK   |      SH2      |       C75      |      SH3      |    C390 C79    |       SH4       |       C405      |
| i.MX6SX Sabre-SD | SH10 (lumped) |    C243 C242   | SH10 (lumped) |    C261 C260   |       R375      |       C265      |
|    i.MX6UL EVK   | R729 (lumped) |      C106      | R729 (lumped) |      C116      |       R730      |       C120      |
|  i.MX7D Sabre-SD |      R20      |        -       |      R24      |        -       |        -        |        -        |

# Sensor board

The [Hi-Lo Current Sensor Combo Board](https://community.nxp.com/docs/DOC-332670)
is a component that eases the process of measuring power consumption. This board
is intended to facilitate the current sensing and can measure currents up to 3A.

The sensor board enables the measurement of the voltage drop over the sense resistor
and with this information it is possible to calculate the current flowing through
the resistor using Ohm's Law:

```
I_rail = V_drop / R_sense
```

This board also allows to measure the power supply rail voltage. With these values,
it is possible to calculate the power consumption on each power rail.

```
P_rail = V_rail * I_rail
```

[^1]: Picture from the [i.MX 6Dual/Quad Power Consumption Measurement](https://cache.freescale.com/files/32bit/doc/app_note/AN4509.pdf) App Note.
[^2]: Information from the [Measuring Current in i.MX Applications](http://www.nxp.com/assets/documents/data/en/application-notes/AN5381.pdf) App Note.
