---
title: IMX Smart-Boot Tool
date: 2017-10-05 19:51:57
author: martin
categories: [ Tools ]
tags: [ VYBRID, IMX6, IMX7, boot_rom, serial_downloader ]
---

All i.MX devices support several options for booting the program image. End 
products usually have eMMC, NAND and NOR Flash chips as preferred boot storages.
The procedures for flashing the program image into this storage chips are too 
complex, differ with the storage type and could depend on a special HW. Therefore, 
for developing or testing purposes a SD card is usually preferred as the main 
boot storage. All what is needed for writing a custom image into SD card is a 
SD card reader (a cheap peace of external HW which many of you already have). 
The only limitation is the process of preparation the boot SD card what is handled 
manually and can take a significant time of the developing process. This tutorial 
will describe a boot option which is eliminationg a preparation of boot storage.

## Serial Downloader

The Serial Downloader provides a way to download a program image to the chip over 
the USB or serial interface. It is implemented in all i.MX6/7 and Vybrid devices 
and it gives a control over the SoC boot process. The description of Serial 
Download Protocol (SDP) and the condition on how to execute the SoC into Serial 
Downloader boot mode are located in the 
[Reference Manual](https://imxdev.gitlab.io/imxdoc/reference-manual-imx6/) 
of the i.MX device. In most cases the i.MX devices set the Serial Downloader as 
a backup option (you will get into Serial Downloader mode if all configured boot 
options return false). Writing your own PC application that implements SDP and 
covers all features of Serial Downloader is not an easy task. But you do not have 
to do it by yourself. 
Boundary Devices have created one for you, 
[IMX USB Loader](https://imxdev.gitlab.io/tutorial/Using_the_imx_usb_loader_tool). 

I believe that in most cases it will give you all that you need, but for exacting 
developers I have created 
[IMX Smart-Boot Tool](https://github.com/molejar/pyIMX/blob/master/doc/imxsb.md). 

This tool adds features for an easy modification or fix to the boot binaries 
like: 
* i.MX Device Configuration Data
* U-Boot Environment Variables
* Device Tree Data

This will save you a lot of time, because you don't need to recompile 
U-Boot or Kernel to apply this kind of changes. IMX Smart-Boot Tool is completely 
written in Python 3.x, which eliminates the compilation process and make it 
platform independent (Windows and Linux are already supported, MacOS will be 
later). 

## How to install IMX Smart-Boot Tool

This tool is distributed with the Python i.MX module. All you need to 
do is to install the [pyIMX](https://github.com/molejar/pyIMX).

## Usage

The basic description on how to use this tool is on the project page 
[IMX Smart-Boot Tool](https://github.com/molejar/pyIMX/blob/master/doc/imxsb.md). 
At this moment there is an example available for i.MX7D SabreSD. More examples 
for other i.MX6/7 targets will be added later.

In general, you can select the following boot options using IMX Smart-Boot Tool.

### USB-OTG boot with RAMDisk image or RootFS mounted via USBNet interface

<img src="{{ site.url }}{{ site.baseurl }}/images/usb_otg_boot_with_ramdisk.png">

### USB-OTG boot with RootFS mounted from SD Card or USB Disk

<img src="{{ site.url }}{{ site.baseurl }}/images/usb_otg_boot_with_rootfs_on_sd.png">

### USB-OTG boot with RootFS mounted via Ethernet interface as NAS

<img src="{{ site.url }}{{ site.baseurl }}/images/usb_otg_boot_with_rootfs_on_nas.png">



