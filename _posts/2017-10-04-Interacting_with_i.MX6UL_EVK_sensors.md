---
title: Interacting with i.MX6UL EVK sensors
date: 2017-10-04 09:21:46
author: breno
categories: [Tutorial]
tags: [IMX6UL, sensor, MAG3110, FXLS8471Q, I2C, L4.1.15_2.0.0_GA]
---

The imx6ulevk baseboard includes the following NXP sensors connected to the
`I2C1` bus.

* [MAG3110: High Accuracy, 3D Magnetometer](https://www.nxp.com/products/sensors/magnetic-sensors/magnetometers/high-accuracy-3d-magnetometer:MAG3110)
* [FXLS8471Q: ±2g/±4g/±8g, Low g, 14-Bit Digital Accelerometer](https://www.nxp.com/products/sensors/motion-sensors/accelerometers-for-iot/2g-4g-8g-low-g-14-bit-digital-accelerometer:FXLS8471Q)

This post shows how to enable the sensors and read their data using the Linux
terminal.

{: .notice--warning}
This tutorial is based in the tag rel_imx_4.1.15_2.0.0_ga on
[linux-imx](http://git.freescale.com/git/cgit.cgi/imx/linux-imx.git/)
repository.

## Configuring kernel to add the sensors

In menu configuration or in the defconfig file enable the sensors driver. (This
step is not necessary when building with imx_v7_defconfig):

```
SENSORS_MAG3110
 Device Drivers > Hardware Monitoring support > Freescale MAG3110 e-compass
sensor

SENSOR_FXLS8471
 Device Drivers > Input device support > Generic input layer > Miscellaneous
devices > FXLS8471 motion sensor device driver.
```

## Enabling the sensors

The sensors are disabled by default, once your board is running it's necessary
to enable the sensors by running the following command lines:

```
Enabling MAG3110 eCOMPASS:
root@imx6ulevk:~# echo 1 > /sys/devices/virtual/input/input3/enable

Enabling FXLS8471Q accelerometer:
root@imx6ulevk:~# echo 1 > /sys/devices/virtual/misc/FreescaleAccelerometer/enable
mma enable setting actived
```
## Reading sensors data

The sensors can be tested in the Linux terminal through `evtest` or by
reading the sensors raw data. Run the commands below and move your board in
different directions to interact with the sensor.

### Reading MAG3110 eCOMPASS through evtest

During this example, please use the board as a compass. The outputted
coordinates (x,y,z) represent the vector pointing to the North.

```
root@imx6ulevk:~# evtest /dev/input/event3

Input driver version is 1.0.1
Input device ID: bus 0x18 vendor 0x0 product 0x0 version 0x0
Input device name: "mag3110"
Supported events:
  Event type 0 (EV_SYN)
  Event type 3 (EV_ABS)
    Event code 0 (ABS_X)
      Value   -987
      Min   -15000
      Max    15000
    Event code 1 (ABS_Y)
      Value   -921
      Min   -15000
      Max    15000
    Event code 2 (ABS_Z)
      Value    519
      Min   -15000
      Max    15000
Properties:
Testing ... (interrupt to exit)
Event: time 1476799745.395772, type 3 (EV_ABS), code 1 (ABS_Y), value -929
Event: time 1476799745.395772, type 3 (EV_ABS), code 2 (ABS_Z), value 524
Event: time 1476799745.395772, -------------- SYN_REPORT ------------
Event: time 1476799745.517924, type 3 (EV_ABS), code 0 (ABS_X), value -933
Event: time 1476799745.517924, type 3 (EV_ABS), code 1 (ABS_Y), value -931
Event: time 1476799745.517924, type 3 (EV_ABS), code 2 (ABS_Z), value 513
Event: time 1476799745.517924, -------------- SYN_REPORT ------------
Event: time 1476799745.636064, type 3 (EV_ABS), code 0 (ABS_X), value -863
Event: time 1476799745.636064, type 3 (EV_ABS), code 1 (ABS_Y), value -968
Event: time 1476799745.636064, type 3 (EV_ABS), code 2 (ABS_Z), value 495
```
### Reading FXLS8471Q accelerometer raw data

As an alternative to `evtest` the sensors raw data can be read, for this example
we are going to use a loop to read the data every 0.5s.

The value measured is the acceleration in each coordinate.

```
root@imx6ulevk:~# while true; do usleep 500000 | cat
/sys/devices/virtual/misc/FreescaleAccelerometer/data; done
240,64,-16496
256,48,-16512
224,64,-16424
392,-984,-17400
-3760,24,-7896
-2016,-7088,-10208
2360,2776,-15864
376,-256,-14448
176,-40,-16528
304,-32,-15944
```
